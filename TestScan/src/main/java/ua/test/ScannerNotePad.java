
public class ScannerNotePad {

	public static void main(String[] args) {

		String delimeter = "=";
		String[] server, endPoint, user, password, format;
		System.out.println();
		for (int i = 0; i < args.length; i++) {

			switch (i) {
			case 0:
				server = args[i].split(delimeter);
				for (int j = 0; j < server.length; j++) {
					System.out.print(server[j] + " ");
				}
				System.out.println();
				break;
			case 1:
				endPoint = args[i].split(delimeter);
				for (int j = 0; j < endPoint.length; j++) {
					System.out.print(endPoint[j] + " ");
				}
				System.out.println();
				break;
			case 2:
				user = args[i].split(delimeter);
				for (int j = 0; j < user.length; j++) {
					System.out.print(user[j] + " ");
				}
				System.out.println();
				break;
			case 3:
				password = args[i].split(delimeter);
				for (int j = 0; j < password.length; j++) {
					System.out.print(password[j] + " ");
				}
				System.out.println();
				break;
			case 4:
				format = args[i].split(delimeter);
				for (int j = 0; j < format.length; j++) {
					System.out.print(format[j] + " ");
				}
				System.out.println();
				break;
			}
		}
		System.out.println();
	}
}
