package com.test.swing;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class TableDemo extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 1L;

	Object[] headers = { "Name", "Surname", "Telephone", "File" };

	Object[][] data = { { "John", "", "1112221", "" }, { "Ivan", "", "2221111", "" }, { "George", "", "3334444", "" },
			{ "Bolvan", "", "2235111", "" }, { "Serg", "", "2221511", "" }, { "Pussy", "", "2221111", "" },
			{ "Tonya", "", "2121111", "" }, { "Elise", "", "2321111", "" }, };

	JTable table;
	private DefaultTableModel model = new DefaultTableModel(data, headers);
	private JFileChooser chooser;
	RowColorRenderer rowRenderer;

	TableDemo() {

		JFrame frame = new JFrame("JTableExample");
		
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setSize(500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		table = new JTable(data, headers);
		table.setModel(model);

		table.addMouseListener(new MouseAdapter() {
			@Override 
			public void mouseClicked(MouseEvent e) {
				int row = table.rowAtPoint(e.getPoint());
				int column = table.columnAtPoint(e.getPoint());
				if (row >= 0 && column == 3) {
					chooser = new JFileChooser();
					FileNameExtensionFilter filter = new FileNameExtensionFilter("Only  .txt  files", "txt");
					chooser.setFileFilter(filter);
					int result = chooser.showOpenDialog(TableDemo.this);
					if (result == JFileChooser.APPROVE_OPTION) {
						table.setValueAt(chooser.getSelectedFile().getName(), row, column);
					}
				}
			}
		});

		for (int i = 0; i < table.getRowCount(); i++) {
			rowRenderer = new RowColorRenderer(i);
			TableColumn column = table.getColumnModel().getColumn(1);
			column.setCellRenderer(rowRenderer);
		}

		JScrollPane scrollPane = new JScrollPane(table);

		table.setPreferredScrollableViewportSize(new Dimension(400, 200));
		frame.getContentPane().add(scrollPane);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	private class RowColorRenderer extends DefaultTableCellRenderer {

		private static final long serialVersionUID = 1L;

		RowColorRenderer(int colNo) {}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {

			switch (table.convertRowIndexToModel(row)) {
			case 0:
				setBackground(new Color(0, 255, 0));
				break;
			case 1:
				setBackground(new Color(255, 0, 0));
				break;
			case 2:
				setBackground(new Color(0, 255, 255));
				break;
			case 3:
				setBackground(new Color(255, 255, 0));
				break;
			case 4:
				setBackground(new Color(128, 128, 128));
				break;
			case 5:
				setBackground(new Color(255, 255, 255));
				break;
			case 6:
				setBackground(new Color(255, 200, 0));
				break;
			case 7:
				setBackground(new Color(255, 0, 255));
				break;
			}
			return this;
		}
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new TableDemo();
			}
		});
	}
}
