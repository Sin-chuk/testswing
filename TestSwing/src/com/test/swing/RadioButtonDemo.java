package com.test.swing;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class RadioButtonDemo implements ItemListener {

	JRadioButton r1, r2;

	RadioButtonDemo() {

		JFrame frame = new JFrame("Frame whith radioButtons");
		frame.setLayout(new FlowLayout());
		frame.setSize(400, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		r1 = new JRadioButton("RadioButton R1");
		r1.addItemListener(this);
		r1.setSelected(true);
		r2 = new JRadioButton("RadioButton R2");
		r2.addItemListener(this);
		ButtonGroup group = new ButtonGroup();

		group.add(r1);
		group.add(r2);

		frame.add(r1);
		frame.add(r2);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true); 
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		JRadioButton r = (JRadioButton) e.getItem();

		Object source = e.getItemSelectable();

		if (source == r1) {
			if (r.isSelected())
				r.setText("R1 is select!");
			else
				r.setText("R1 is not select!");

		} else {
			if (r.isSelected())
				r.setText("R2 is select!");
			else
				r.setText("R2 is not select!");
		}
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new RadioButtonDemo();
			}
		});
	}
}
