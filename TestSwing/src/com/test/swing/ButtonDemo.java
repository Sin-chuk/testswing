package com.test.swing;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ButtonDemo implements ActionListener {
	JLabel label;

	ButtonDemo() {

		JFrame frame = new JFrame("Frame whith buttons");
		frame.setLayout(new FlowLayout());
		frame.setSize(200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton button1 = new JButton("button1");
		button1.setBackground(Color.WHITE);
		button1.setForeground(Color.RED);
		JButton button2 = new JButton("button2");
		button2.setBackground(Color.GRAY);
		button2.setForeground(Color.BLUE);

		button1.addActionListener(this);
		button2.addActionListener(this);

		frame.add(button1);
		frame.add(button2);

		label = new JLabel("Press any key");
		frame.add(label);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getActionCommand().equals("button1")) {
			label.setForeground(Color.RED);
			label.setText("You pressed button1.");
		} else {
			label.setForeground(Color.BLUE);
			label.setText("You pressed button2.");
		}
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new ButtonDemo();
			}

		});
	}
}
