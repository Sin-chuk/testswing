package com.test.swing;

import java.awt.*;
import javax.swing.*;

public class FlowDemo {

	FlowDemo() {

		JFrame frame = new JFrame("Frame whith different Flowes");

//		frame.setLayout(new BorderLayout());

		frame.setLayout(new FlowLayout(0, 100, 100));

//		frame.setLayout(new GridBagLayout());

		frame.setSize(500, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
//		panel.setLayout(new GridLayout(3, 2));
		panel.setLayout(new GridBagLayout());
		GridBagConstraints constraint = new GridBagConstraints();
		JButton button1 = new JButton("Button 1");
		constraint.weightx = 0.5;
		constraint.gridx = 0;
		constraint.gridy = 0;
		panel.add(button1, constraint);
		JButton button2 = new JButton("Button 2");
		constraint.weightx = 0.5;
		constraint.gridx = 1;
		constraint.gridy = 0;
		panel.add(button2, constraint);
		JButton button3 = new JButton("Button 3");
		constraint.weightx = 0.5;
		constraint.gridx = 2;
		constraint.gridy = 0;
		panel.add(button3, constraint);
		JButton button4 = new JButton("Button 4");
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.ipady = 40;
		constraint.weightx = 0.0;
		constraint.gridwidth = 3;
		constraint.gridx = 0;
		constraint.gridy = 1;
		panel.add(button4, constraint);
//		JButton button5 = new JButton("Button 5");
//		JButton button6 = new JButton("Button 6");

//		panel.add(button1);
//		panel.add(button2);
//		panel.add(button3);
//		panel.add(button4);
//		panel.add(button5);
//		panel.add(button6);

		frame.add(panel);

//		frame.add(button1, BorderLayout.WEST);
//		frame.add(button2, BorderLayout.CENTER);
//		frame.add(button3, BorderLayout.SOUTH);
//		frame.add(button4, BorderLayout.NORTH);
//		frame.add(button5, BorderLayout.EAST);

//		frame.add(button1);
//		frame.add(button2);
//		frame.add(button3);
//		frame.add(button4);
//		frame.add(button5);

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new FlowDemo();
			}
		});
	}
}
