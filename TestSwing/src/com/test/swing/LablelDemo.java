package com.test.swing;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;

public class LablelDemo {

	public LablelDemo() {

		JFrame frame = new JFrame("Frame whith lable");
		frame.setLayout(new FlowLayout(0, 50, 50));

		frame.setSize(900, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel lable1 = new JLabel("test Lable 1");

		Border border1 = BorderFactory.createLineBorder(Color.RED, 5);
		lable1.setBorder(border1);
		lable1.setPreferredSize(new Dimension(150, 100));
		lable1.setHorizontalAlignment(JLabel.CENTER);
		lable1.setVerticalAlignment(JLabel.CENTER);

		JLabel lable2 = new JLabel("test Lable 2");
		Border border2 = BorderFactory.createMatteBorder(10, 20, 50, 5, Color.BLUE);
		lable2.setBorder(border2);
		lable2.setPreferredSize(new Dimension(220, 100));
		lable2.setHorizontalAlignment(JLabel.CENTER);
		lable2.setVerticalAlignment(JLabel.CENTER);

		JLabel lable3 = new JLabel("test Lable 3");
		Border border3 = BorderFactory.createBevelBorder(0);
		lable3.setBorder(border3);
		lable3.setPreferredSize(new Dimension(100, 50));
		lable3.setHorizontalAlignment(JLabel.CENTER);
		lable3.setVerticalAlignment(JLabel.CENTER);

		JLabel lable4 = new JLabel("test Lable 4");
		Border border4 = BorderFactory.createDashedBorder(Color.GREEN, 3, 5, 1, true);
		lable4.setBorder(border4);
		lable4.setPreferredSize(new Dimension(100, 50));
		lable4.setHorizontalAlignment(JLabel.CENTER);
		lable4.setVerticalAlignment(JLabel.CENTER);

		frame.add(lable1);
		frame.add(lable2);
		frame.add(lable3);
		frame.add(lable4);
		
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new LablelDemo();
			}

		});
	}
}
