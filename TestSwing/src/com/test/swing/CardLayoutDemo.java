package com.test.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CardLayoutDemo implements ItemListener {
	JPanel cards;
	final static String BUTTONPANEL = "Card with JButtons";
	final static String TEXTPANEL = "Card with JTextField";
	final static String LABLEPANEL = "Card with JLabel";

	public void addComponentToPane(Container pane) {

		JPanel comboBoxPane = new JPanel();
		String comboBoxItems[] = { BUTTONPANEL, TEXTPANEL, LABLEPANEL };
		JComboBox<String> comboBox = new JComboBox<>(comboBoxItems);
		comboBox.setEditable(false);
		comboBox.addItemListener(this);
		comboBoxPane.add(comboBox);

		JPanel card1 = new JPanel();
		card1.add(new JButton("Button 1"));
		card1.add(new JButton("Button 2"));
		card1.add(new JButton("Button 3"));

		JPanel card2 = new JPanel();
		card2.add(new JTextField("TextField", 20));
		
		JPanel card3 = new JPanel();
		card3.add(new JLabel("Label with text"));

		cards = new JPanel(new CardLayout());
		cards.add(card1, BUTTONPANEL);
		cards.add(card2, TEXTPANEL);
		cards.add(card3, LABLEPANEL);

		pane.add(comboBoxPane, BorderLayout.PAGE_START);
		pane.add(cards, BorderLayout.CENTER);
	}

	@Override
	public void itemStateChanged(ItemEvent evt) {
		CardLayout cardLayout = (CardLayout) (cards.getLayout());//return layoutMgr
		cardLayout.show(cards, (String) evt.getItem());
	}

	private static void createAndShowGUI() {

		JFrame frame = new JFrame("CardLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		CardLayoutDemo cardsLayoutDemo = new CardLayoutDemo();
		cardsLayoutDemo.addComponentToPane(frame.getContentPane());

		frame.setSize(400, 200);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
