package com.test.swing;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CheckBoxDemo implements ItemListener {
	JCheckBox checkBox1, checkBox2;

	CheckBoxDemo() {

		JFrame frame = new JFrame("Frame whith checkBox");
		frame.setLayout(new GridLayout(1, 0, 70, 10));
		frame.setSize(600, 160);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		checkBox1 = new JCheckBox("Turn checkBox 1");
		checkBox1.addItemListener(this);
		checkBox2 = new JCheckBox("Turn checkBox 2");
		checkBox2.addItemListener(this);

		frame.add(checkBox1);
		frame.add(checkBox2);
		
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		JCheckBox checkBox = (JCheckBox) e.getItem();
		Object source = e.getItemSelectable();

		if (source == checkBox1) {
			if (checkBox.isSelected()) {
				checkBox.setForeground(Color.RED);
				checkBox.setText("Thanks for turn ON checkBox 1 !");
			} else {
				checkBox.setForeground(Color.BLUE);
				checkBox.setText("Thanks for turn OFF checkBox 1!");
			}

		} else {
			if (checkBox.isSelected()) {
				checkBox.setForeground(Color.RED);
				checkBox.setText("Thanks for turn ON checkBox 2 !");
			} else {
				checkBox.setForeground(Color.BLUE);
				checkBox.setText("Thanks for turn OFF checkBox 2!");
			}
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new CheckBoxDemo();
			}
		});
	}

}
